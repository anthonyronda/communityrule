---
layout: page
title: About
permalink: /about/
---

<span style="font-size: 1.2em">CommunityRule is a governance toolkit for great communities.</span>

<em>"For everyone to have the opportunity to be involved in a given group and to participate in its activities the structure must be explicit, not implicit. The rules of decision-making must be open and available to everyone, and this can happen only if they are formalized."</em> (Jo Freeman, "The Tyranny of Structurelessness")

Too many of our communities adopt default governance practices that rely on the unchecked authority of founders, admins, or moderators, lacking even basic features of small-scale democracy. The purpose of CommunityRule is to help communities establish appropriate norms for decision-making, stewardship, and culture.

<strong>More background information is at the [FAQ]({% link _about/faq.md %}), and use the [Guides]({% link guides.md %}) to get started making a Rule of your own.</strong>

The governance practices we share here have long roots in diverse cultures. To ensure this inheritance remains a commons, content on CommunityRule is licensed under a [Creative Commons Attribution-ShareAlike 4.0 International License](https://creativecommons.org/licenses/by-sa/4.0/), which means that anyone has the right to use and adapt it, and adaptations must be re-shared under the same license.

CommunityRule is a project of the [Media Enterprise Design Lab](https://www.colorado.edu/lab/medlab/) at the University of Colorado Boulder, in collaboration with the [Metagovernance Project](https://metagov.org).

CommunityRule is currently administered and primarily authored by MEDLab director Nathan Schneider. If a community of contributors and users forms around CommunityRule, it should develop into a more community-driven governance model with the help of CommunityRule itself.

To contribute to the project, post Issues and Merge Requests to the [project on GitLab](https://gitlab.com/medlabboulder/communityrule), or send an email to medlab@colorado.edu.
