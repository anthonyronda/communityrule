---
layout: guide
title: "Rules for Mutual Aid Groups"
permalink: /guides/mutual-aid/
---

"Mutual aid" groups generally seek to address community challenges
through peer relationships and an eye toward transformative social
change. They seek to operate by the dictum "solidarity, not charity."
Adopting clear governance plans early can increase the effectiveness and
sustainability of mutual aid groups. Undefined means of operating can
lead to what feminist scholar Jo Freeman refers to as a "[tyranny of
structurelessness](https://www.jofreeman.com/joreen/tyranny.htm)." This
concept addresses the potential of unacknowledged power relations to
dominate a group's decision making process. Without structure there is
no means to question de facto leadership or hierarchies.

Here are some ideas about how to start:

**Don't overdo it.** Too many rules and documents can stifle volunteer
groups. Regulations should be mostly invisible, except when they're
needed. The vast majority of good governance work is not rules or tech:
it is culture, communication, and trust. Keep your focus on those, but
have the rules in the background.

**Make explicit where you are.** [Create your first
Rule](https://communityrule.info/guides/first-rule/), using the
[Create]({% link create.md %}) tool, to determine and
articulate how your group currently functions. Pick a template that
closely reflects where your group currently stands.

-   Be honest with yourself and your community
-   Don't claim to do what you are not ready for
-   Don't get stuck on utopianism, use it as a mirror

**Introduce achievable commitments to strive toward.** Draw from the
[Templates]({% link templates.md %}) to think about what
you might like to have in your Rule in the future, filing in the current
gaps with reasonable additions. Consider adding---either in your Rule or
linked from it---a code of conduct, community agreement, safer space
policy or the like.

-   Be sure all the main questions are answered
-   Reflect on the values that are widespread in the group
-   Share a draft for feedback
-   Devise an appropriate ratification process
-   Ensure there is a mechanism for modifying the Rule---it should be
    harder than most decisions, but not out of reach

**Honor your commitments.** As specified in your Rule, make sure you
have clear mechanism for carrying out all governance procedures and
resolving issues that arise. Enforce any agreements about behavior
fairly and uniformly.

-   Share governance roles as widely as possible without creating
    burdens
-   Account for members' needs and time commitments, recognizing that
    not everyone will be able to attend every meeting and enabling
    multiple methods for participation
-   If possible, separate out enforcement and rule-making roles
-   Be prepared to allow changes to the Rule as experience requires
-   Keep the focus on the work

Designing governance for volunteer-powered groups can be challenging.
Focus on transparency and fairness. And remember: culture,
communication, and trust are vital elements of successful
self-governing.
