---
layout: rule
title: Elected board
permalink: /templates/elected_board/
icon: /assets/tabler_icons/user-check.svg

community-name:

# BASICS
structure: An annually elected board of no more than 5 members determines policies and implements them.
mission:
values:
legal: 

# PARTICIPANTS
membership: The board's policies determine the criteria for community membership.
removal: The board may choose to remove a community member if at least 51% of board members vote for removal.
roles:
limits:

# POLICY
rights: The board is expected to operate according to this Rule and the explicit agreements it has created.
decision: Each year, community members vote on nominees for board positions. The 5 nominees with the largest number of votes become board members for the subsequent year. Current board members propose and vote on agreements by a secret ballot. At least 51% of board members must vote to approve a proposal in order for it to become a community agreement.
implementation: Board members or their designees are responsible for implementing agreements, and the board should have access to the means to implement the agreements that it has approved.
oversight: Community members unhappy with the board's implementation with the agreements may seek to replace board members at the next election.
limits: Board members can serve an unlimited number of 1-year terms.

## PROCESS
access:
economics:
deliberation:
grievances:

# EVOLUTION
records:
modification: Changes to this Rule require 51% of the board to approve a referendum circulated to the entire community. The change passes if it receives 2/3 approval of all who vote after one week.
---

Use this template as-is or edit it to adapt it to your community.

A widely used governance structure for membership organizations whose members are informed about governance processes but don't have capacity for active involvement.
