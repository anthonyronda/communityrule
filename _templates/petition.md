---
layout: rule
title: Petition
permalink: /templates/petition/
icon: /assets/tabler_icons/clipboard-list.svg

community-name: 

# BASICS
structure: All members have potentially equal voice in a direct democracy. Volunteer community administrators, who are invited by other administrators, carry out the community's decisions.
mission:
values: Every member's voice should have the chance to be heard in decision making.
legal:

# PARTICIPANTS
membership: Anyone may in principle become a member, but administrators can prevent someone from joining if they have reason to suspect bad faith.
removal: A member may be removed if a petition for their removal passes.
roles:
limits: Administrators reserve the right to refrain from implementing decisions they regard as infeasible.

# POLICY
rights: All members have the right to initiate peitions, sign them, and vote in resulting referendums.
decision: All policies and governance actions in the community are initiated by petition. Any member can start a petition. If 1/3 of the membership signs it, the content of the petition becomes a referendum in which all members are invited to vote. If at least 2/3 of the votes are in favor of the petition after 1 week, it passes.
implementation: Administrators are responsible for carrying out the outcomes of the petition process.
oversight: The petition process may be used to remove administrators if they are deemed non-compliant with community policies, and the petition process may also be used to reverse the outcomes of previous processes.

## PROCESS
access:
economics:
deliberation: When a petition goes before the community, the petitioner must organize at least one synchronous discussion session open to all members in the first half of the voting period.
grievances:

# EVOLUTION
records: 
modification: The petition process may be used to modify this Rule.
---

Use this template as-is or edit it to adapt it to your community.

Enables community members to have direct involvement in crafting and deciding on policy.
