---
layout: page
permalink: /
---

<div style="text-align: center">

    <div class="callout-text">
        CommunityRule is a governance toolkit for great communities.
    </div>

    <img src="{% link assets/birds.png %}" alt="Birds image from https://svgsilh.com/image/2022610.html" />

    <div class="callout-text">
        How does your community work?<br />
        Are you ready to make hard decisions?
    </div>

    Too often, we leave the most basic questions unstated and unanswered. CommunityRule makes it easier to clarify the basics so you can focus on other things.

    <div class="callout-icon">
        <img src="{% link assets/tabler_icons/edit.svg %}" width="10%" />
    </div>

    <div class="callout-text">
        <a href="{% link create.md %}">Start from scratch</a> or <a href="{% link templates.md %}">choose a template</a>
    </div>

    <div class="callout-icon">
        <img src="{% link assets/tabler_icons/chart-candle.svg %}" width="10%" />
    </div>

    <div class="callout-text">
        <a href="{% link guides.md %}">Tailor a Rule</a> to your community's needs and values
    </div>

    <div class="callout-icon">
        <img src="{% link assets/tabler_icons/license.svg %}" width="10%" />
    </div>

    <div class="callout-text">
        Then publish your Rule and <a href="{% link library.md %}">share it with others</a>
    </div>

</div>
