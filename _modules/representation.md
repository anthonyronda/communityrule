---
layout: module
title: Representation
permalink: /modules/representation/
summary: Rather than decide directly, people choose representatives to participate in decision making on their behalf.
type: process
---
