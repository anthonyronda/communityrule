---
layout: module
title: Quadratic Voting
permalink: /modules/quadratic_voting/
summary: Voters pay to vote, but the cost of voting power increases the more a voter buys.
type: decision
---

Quadratic voting (QV) is a decision-making mechanism that allows voters to pay money or other credits to express their priorities. The more credits a voter applies to a given option, the less each credit counts, according to a quadratic decay function.

The QV proposals involve several mechanisms to adjust for the power of wealth concentrations, such as distributing credits equally or redistributing the proceeds of credit contributions for future ballots.

**Input:** ballot of options, weight (e.g., money or pre-assigned credits), quadratic decay function

**Output:** weighted outcome, redistribution of weight credits

## Background

Quadratic voting was first proposed by Steven Lalley and E. Glen Weyl in 2012 and has been developed in several publications since. It has been widely discussed especially among people involved in blockchain protocol design. The concept is patented by Weyl and collaborators through the company Collective Decision Engines. Weyl, through economic analysis, argues that QV has uniquely optimal outcomes when compared to other decision-making systems.


## Feedback loops

### Sensitivities

* Reflecting degrees of preference
* Dampening influence of excessive wealth

### Oversights

* Perceived complexity of quadratic algorithm
* Preferences of less credit-endowed participants, under certain implementations

## Implementations

### Communities

* Colorado state legislature used it to determine priorities
    - Press: _[Wired](https://www.wired.com/story/colorado-quadratic-voting-experiment/)_, _[Colorado Sun](https://coloradosun.com/2019/05/28/quadratic-voting-colorado-house-budget/)_

### Tools

* [Democracy Earth](https://democracy.earth) has implemented a quadratic voting tool

## Further resources

*  Lalley, Steven and Weyl, Eric Glen. "[Quadratic Voting: How Mechanism Design Can Radicalize Democracy](https://papers.ssrn.com/sol3/papers.cfm?abstract_id=2003531)." _American Economic Association Papers and Proceedings_ 1, no. 1 (2018)
* Posner, Eric A. and E. Glen Weyl. _Radical Markets: Uprooting Capitalism and Democracy for a Just Society_. Princeton University Press, 2018.
* Weyl, E. Glen. "[The robustness of quadratic voting](https://link.springer.com/article/10.1007/s11127-017-0405-4)." _Public Choice_ 172, no. 1-2 (July 2017)
* Weyl, W. Glen et al. "[System and Method for Quadratic, Near-Quadratic, and Convex Voting in Various Types of Research and Voting](https://worldwide.espacenet.com/publicationDetails/biblio?CC=US&NR=9754272&KC=&FT=E&locale=en_EP#)." US patent US2016292947. October 6, 2016.
