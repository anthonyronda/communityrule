---
layout: module
title: Friendship
permalink: /modules/friendship/
summary: Relationships among participants facilitate or inhibit governance processes.
type: culture
---

Friendship is an association based on mutual affection, respect, and equality. Specific elements at play may vary, but in general, friendship is built upon a foundation of positive feelings between two parties. In most cases, friendship develops due to common interests, values, and beliefs. 

**Input:** shared respect and notions of equality

**Output:** mutually beneficial, horizontal relationship between parties

## Background

Differing ideas friendship date back to classic thinkers like Cicero and Aristotle. For Cicero, friendship was a phenomena built on virtue – it was an opportunity for individuals to find another self, to feel rewarded from the friendship and kinship itself. Cicero’s dialogue “De Amicitia,” or “How to be a Friend,” describes in depth his views on friendship. 

For Aristotle, however, varying types of friendship with differing rewards exist. His first two types of friendship are not intentional. One exists for the utility it provides for the parties while the other exists for the pleasure it provides. Both of these are superficial and short-lasting. The most intentional and enduring type of friendship he defines is based on mutual appreciation for values, beliefs, and opinions – appreciating the other person’s character is what allows this type of friendship to last and to grow.

In a democratic sense, civic or political friendship is valuable for governance and for the state; for Aristotle, citizens should strive for the common good of the state, working towards a common goal and helping one another. In a situation rich with political friendship, revolution and turmoil are kept at bay because citizens strive to live well together.

## Feedback loops

### Sensitivities

* On a basic level, friendship has individuals benefits for [overall health and happiness]( https://www.mindwise.org/blog/mental-health/how-friendship-affects-your-physical-mental-health/)
* Civic friendship leads to an authentic view of others as equals, leading to an increased willingness to participate in practices that encourage equality on an institutional level 
* Friendship between states and countries can reduce international strife and increase respect for cultural difference
* Civic friendship can create a robust community of individuals taking civic action

### Oversights

* In any governance setting, including places of work, true equality and a genuine desire to work toward a common good can be difficult to achieve given the hierarchical structure of many companies
* A modern, Western focus on individualism and selfhood diverts focus from interactions with others for a wider goal and encourages Aristotle’s first two forms of friendship
* Classical notions of civic friends exclude marginalized groups such as people of color and women; truly equitable relationships are difficult to form in an unjust society
* Friendship can foster a utilitarian, or “best action for best number of people” approach rather than addressing complexities of the political system

## Implementations

### Communities

Civic friendship can manifest in [civic engagement]( https://www.everyday-democracy.org/news/civic-engagement-three-examples-where-citizens-had-say), such as an effort to stop gerrymandering in California taken on by the California Citizens Redistricting Commission or the Strong Starts for Children program formed in New Mexico. 

### Tools

* The Dole Institute offers a [list of tools for civic engagement]( http://doleinstitute.org/get-involved/civic-engagement-tools/) – a concrete manifestation of civic friendship.
* The U.S. Department of Education’s [Sustaining Grassroots Community-Based Programs: A Toolkit for Community- and Faith-Based Service Providers](https://sites.ed.gov/aapi/files/2014/03/SAMHSA-Toolkit.pdf).
* To build community and friendships within and around your organization, John Snow, Inc.’s [Engaging your Community: A Toolkit for Partnership, Collaboration, and Action](https://www.jsi.com/JSIInternet/Inc/Common/_download_pub.cfm?id=14333&lid=3) is available for download.

## Further resources

Shukla, R. (2014). [Justice and Civic Friendship: An Aristotelian Critique of Modern Citizenry]( http://www.jstor.org.colorado.idm.oclc.org/stable/43281396). Frontiers of Philosophy in China,9(1), 1-20.

Mayhew, R. (1996). [Aristotle on Civic Friendship]( https://pdfs.semanticscholar.org/5763/169fafa5dc4dc917a59e4fa71162b64358aa.pdf). The Society for Ancient Greek Philosophy Newsletter, 197. 

Schwarzenbach, S. A. (2005). Democracy and friendship. Journal of Social Philosophy, 36(2), 233-254. doi:10.1111/j.1467-9833.2005.00269.x
