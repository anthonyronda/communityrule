---
layout: module
title: Consensus
permalink: /modules/consensus/
summary: All participants must agree on decisions that affect the entire group.
type: decision
---

# Consensus process

Consensus is a form of group decision-making that reflects the perceived best possible scenario for the greatest number of people. If an individual or a minority cannot support the decision, it can be blocked. Consensus and unanimity are not the same; the decision-rule decided upon by the organization provides the threshold for how many individuals  or groups must agree to the consensus for it to be implemented.

**Input:** a group of individuals willing to collaborate and compromise for a decision with an eye toward unity; individuals with diverse delegated roles like facilitator, timekeeper, and notetaker (though positions and responsibilities may vary among groups); a decision-rule agreed upon by the organization

**Output:** a decision that addresses the needs of the greatest number of people in the group based on compromise; an equitable environment for stakeholders

## Background

In Western contexts, consensus as a decision-making process dates back to the 17th century Quakers. Other religious groups have history of consensus as decision making, including Anabaptists. A process similar, though not exactly reflective of modern consensus, was prevalent among indigenous groups like the [Ayamara, Haudenosaunee, and Saharan San bushmen]( https://rhizomenetwork.files.wordpress.com/2010/12/history_of_consensus_jan2012_v2.pdf). These groups had governance processes that strove to address the needs of the greatest number of people through participatory culture. 

The 1960’s and ‘70’s demonstrated modern consensus process in the United States through social movements like the Civil Rights Movement and Women’s Movement. Later, movements like the World Trade Organization protests in Seattle and Occupy movement around the globe established a strong foundation in consensus decision-making.

## Feedback loops

### Sensitivities

* Strives to be inclusive and egalitarian, encouraging all members of the group to collaborate in decision forming
* Protects minority opinions
* Provides an opportunity for a win-win situation through community team-building that occurs through productive dialogue involved in the consensus process
* Encourages all members to have a stake in the decision

### Oversights

* Individuals might feel fearful and discouraged to express their real beliefs or concerns on an issue if a majority already agrees on a decision
* Marginalized groups’ opinions and beliefs may be demonized and their opinions ignored
* The decision that is most fair may not actually be the best decision for the group; compromise could exclude important aspects
* In business setting, hierarchy and seniority may amplify certain employees’ voices and undermine others’

## Implementations

### Communities

While consensus is still used in social movements, many co-operative businesses find it an important hallmark of their organizational decision-making. 
* The [Boulder Housing Coalition]( https://boulderhousingcoalition.org/resources/consensus/) in Colorado is a co-op that uses consensus as a self-governing process.
* [Ottawa Valley Food Co-Op]( https://www.ottawavalleyfood.org/images/PDF/OVFC%20Business%20Plan%20-%20Full%20Draft%20-%20October%2018%202015.pdf) in Canada uses consensus decision-making
* [Radical Routes]( https://www.radicalroutes.org.uk/aims-and-principles.html) is a “network of housing and worker co-operatives working for radical social change” in Britain that operates primary on consensus decisions by all network members.

### Tools

* (ConsensusDecisionMaking.org) offers a one-hour training video on the basic principles of consensus
* A [checklist]( https://leadtogether.org/wp-content/uploads/2014/06/COnsensus-checklist.pdf) for the consensus process
* This [handbook]( https://leadtogether.org/wp-content/uploads/2014/06/on-conflict-and-consensus.pdf) on consensus decision-making

## Further resources

* Loomio, "[Consent decision making improves group outcomes](https://help.loomio.org/en/guides/consent_process/)"
* [Rhizome Guide to A History of Consensus]( https://leadtogether.org/wp-content/uploads/2014/06/on-conflict-and-consensus.pdf). (2010). Rhizome Co-operative. 
* [Consensus Decision Making]( https://www.seedsforchange.org.uk/consensus). Seeds for Change.
* Pérez, I. J., Cabrerizo, F. J., Alonso, S., Dong, Y. C., Chiclana, F., & Herrera-Viedma, E. (2018). On dynamic consensus processes in group decision making problems. Information Sciences, 459, 20-35.
