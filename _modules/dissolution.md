---
layout: module
title: Dissolution
permalink: /modules/dissolution/
summary: Defines a process for how to dissolve the community.
type: process
---