---
layout: module
title: Policy Register
permalink: /modules/policy_register/
summary: A record that documents all currently binding agreements and policies.
type: process
---
