---
layout: module
title: Expiration
permalink: /modules/expiration/
summary: An organization, entity, or process comes to an end.
type: process
---
