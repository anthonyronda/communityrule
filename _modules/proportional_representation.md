---
layout: module
title: Proportional Representation
permalink: /modules/proportional_representation/
summary: Decision makers are chosen in proportion to votes.
type: process
---

Proportional representation (PR) refers to the fair and accurate translation of citizen and party makeup into the legislature. If *x* percent of voters support a certain party, then *x* percent of the legislature should mirror that party. Proportional representation voting systems are comprised of three main variations: the party list system, the mixed-member system, and the single transferable vote (or choice voting).

**Input:** multimember districts, voting base, intentional (often low) exclusion threshold 

**Output:** multiparty representation, proportional allocation of seats

## Background

Proportional representation voting systems came about as a reaction to the inaccurate representation of parties that frequently occurred in the plurality and majority voting systems. In the late 19th and early 20th centuries, many European countries began to realize the need for increasingly diverse parties to have a fair share of seats as suffrage expanded voter pools. The shift to proportional representation occurred for most European countries by 1920; Western democracies mirrored this trend throughout the 20th century.

## Feedback loops

### Sensitivities

* Representation for multiple parties, leading to more nuanced policies
* Representation for marginalized groups
* Potential for smaller parties to gain representation
* Research demonstrates higher voter turnout in PR systems

### Oversights

* May produce a fractured legislature of too many parties
* Can create instability due to lack of majority consensus
* Parties that form coalitions may become difficult to remove from power
* Small parties may have disproportional power in tipping a decision between majority parties
* May create space for extremist parties to gain power

## Implementations

### Communities

Many Western democracies use PR systems, including Sweden, Denmark, Finland, Norway, Netherlands, Germany, Belgium, and Ireland.

### Tools

* Various formulas exist to calculate the allocated number of seats based on election results, including the D’Hondt Method or Jefferson method, Hare method, and German method
* The ACE Project provides extensive information on PR systems and the variations of this system with example ballots and formulas

## Further resources

* ACE Project: The Electoral Knowledge Network. "Proportional Representation."
* Amy, D. J. (2000). _Behind the ballot box: a citizen's guide to voting systems_. Greenwood Publishing Group.

