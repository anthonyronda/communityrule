---
layout: module
title: Stochastic Choice
permalink: /modules/stochastic_choice/
summary: A choice among options is made at random.
type: decision
---
