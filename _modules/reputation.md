---
layout: module
title: Reputation
permalink: /modules/reputation/
summary: A system measures and evaluates participants' behavior and weighs their relative rights or credibility accordingly.
type: process
---


<!-- Chinese citizen score -->
<!-- social media platforms -->
<!-- Aragon -->
<!-- Colony: http://www.erasmuslawreview.nl/tijdschrift/ELR/2019/01/ELR_2018_011_003_005 -->
