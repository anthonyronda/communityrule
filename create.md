---
layout: rule
title: Create
permalink: /create/
icon: 

community-name: 

# BASICS
structure: 
mission:
values:
legal: 

# PARTICIPANTS
membership:
removal:
roles:
limits: 

# POLICY
rights: 
decision: 
implementation: 
oversight: 

# PROCESS
deliberation: 
access: 
economics: 
grievances: 

# EVOLUTION
records: 
modification: 
---

